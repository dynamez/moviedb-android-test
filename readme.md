The MovieDB Test APP

Cree una aplicación en Swift o Kotlin que consuma la api de themoviedb.org. Debe existir una pantalla donde se pueda mostrar la lista de las peliculas mas populares.
 
- A partir de la rama develop, cree una rama feature para esta funcionalidad. Realice todos los commit necesarios y cree un pull request para su union. (Este sera revisado como parte de la revision del test)

- Url para obtener las películas ordenadas por popular: https://api.themoviedb.org/3/movie/popular?api_key=34738023d27013e6d1b995443764da44

- Debe haber un botón en cada ítem de la lista, que al hacer clic se debe mostrar un detalle de la película en una porción de la misma pantalla dedicada para ello.

- Para obtener la imagen/poster de la película, esta es la dirección base http://image.tmdb.org/t/p/w500. Concatene la dirección que aparece en el campo "poster_path" del JSON de la película, de esta forma podrá generar la url completa para obtener la imagen(ej: http://image.tmdb.org/t/p/w500/uC6TTUhPpQCmgldGyYveKRAu8JN.jpg)
 
Consideraciones:
- Sin importar que termines o no la aplicación, preocupate que lo realizado sea de buen nivel.
- Utilice las mejores practicas y las librerias que conozca. (networking, depency injection, etc)
- Realice todos los test unitarios, instrumentales de interfaz que estime necesario.
